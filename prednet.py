import numpy
import chainer
import chainer.functions as F
import chainer.links as L

from convlstm import ConvLSTM

# channels = [3, 48, 96, 192]

class PredNet(chainer.Chain):
    def __init__(self, height,  width, channels, gamma=0.1, nt=10): # CHANGE: added gamma, test, nt
        self.filter_size = 3
        self.channels = channels
        h, w, ch, fs = height, width, channels, self.filter_size
        super(PredNet, self).__init__(
            # l = 0
            RN0        = ConvLSTM(ch[0]*2 + ch[1], ch[0], h, w, nt=nt),
            Conv0_hat  = L.Convolution2D(ch[0],   ch[0], fs, pad=1),
            Conv0      = L.Convolution2D(ch[0]*2, ch[1], fs, pad=1),
            
            # l = 1
            RN1        = ConvLSTM(ch[1]*2 + ch[2], ch[1], h//2**1, w//2**1, nt=nt),
            Conv1_hat  = L.Convolution2D(ch[1],   ch[1], fs, pad=1),
            Conv1      = L.Convolution2D(ch[1]*2, ch[2], fs, pad=1),

            # l = 2
            RN2        = ConvLSTM(ch[2]*2 + ch[3], ch[2], h//2**2, w//2**2, nt=nt),
            Conv2_hat  = L.Convolution2D(ch[2],   ch[2], fs, pad=1),
            Conv2      = L.Convolution2D(ch[2]*2, ch[3], fs, pad=1),
            # l = 3
            RN3        = ConvLSTM(ch[3]*2, ch[3], h//2**3, w//2**3, nt=nt),
            Conv3_hat  = L.Convolution2D(ch[3], ch[3], fs, pad=1),
        
        )


        self.e0 = None
        self.e1 = None
        self.e2 = None
        self.e3 = None

        
        
    def reset_state(self):
        self.RN0.reset_state()
        self.RN1.reset_state()
        self.RN2.reset_state()
        self.RN3.reset_state()
        self.e0 = None
        self.e1 = None
        self.e2 = None
        self.e3 = None
    

    def __call__(self, x, timestep, x_true=None,test=False):# CHANGE: added timestep
        batchsize, n_channels, h, w = x.data.shape
        if self.e3 is None:
            self.e3 = chainer.Variable(
                    self.xp.zeros((batchsize, self.channels[3]*2, h//2**3, w//2**3), dtype=x.data.dtype),
                    volatile='auto')
            self.e2 = chainer.Variable(
                    self.xp.zeros((batchsize, self.channels[2]*2, h//2**2, w//2**2), dtype=x.data.dtype),
                    volatile='auto')
            self.e1 = chainer.Variable(
                    self.xp.zeros((batchsize, self.channels[1]*2, h//2**1, w//2**1), dtype=x.data.dtype),
                    volatile='auto')
            self.e0 = chainer.Variable(
                    self.xp.zeros((batchsize, self.channels[0]*2, h, w), dtype=x.data.dtype),
                    volatile='auto')

        #upsample = lambda x: self.xp.repeat(self.xp.repeat(x,2,axis=2),2,axis=3)
        # update RNN states        
        r3 = self.RN3(self.e3,timestep,test=test)
        r2 = self.RN2(F.concat((self.e2, F.unpooling_2d(r3, 2, stride=2, cover_all=False)), axis=1),timestep,test=test)
        r1 = self.RN1(F.concat((self.e1, F.unpooling_2d(r2, 2, stride=2, cover_all=False)), axis=1),timestep,test=test)
        r0 = self.RN0(F.concat((self.e0, F.unpooling_2d(r1, 2, stride=2, cover_all=False)), axis=1),timestep,test=test)
        # update A_hat, A, and E states
        a0 = x
        a0_hat  = F.clipped_relu(self.Conv0_hat(r0),  1.0)
        if x_true is not None:
            a0 = a0_hat
        self.e0 = F.concat((F.relu(a0 - a0_hat), F.relu(a0_hat - a0)), axis=1)
        
        a1      = F.max_pooling_2d(F.relu(self.Conv0(self.e0)), 2, stride=None, pad=0)
        a1_hat  = F.relu(self.Conv1_hat(r1))
        self.e1 = F.concat((F.relu(a1 - a1_hat), F.relu(a1_hat - a1)), axis=1)
        
        a2      = F.max_pooling_2d(F.relu(self.Conv1(self.e1)), 2, stride=None, pad=0)
        a2_hat  = F.relu(self.Conv2_hat(r2))
        self.e2 = F.concat((F.relu(a2 - a2_hat), F.relu(a2_hat - a2)), axis=1)
        
        a3      = F.max_pooling_2d(F.relu(self.Conv2(self.e2)), 2, stride=None, pad=0)
        a3_hat  = F.relu(self.Conv3_hat(r3))
        self.e3 = F.concat((F.relu(a3 - a3_hat), F.relu(a3_hat - a3)), axis=1)
        
        if x_true is not None:
            errors = F.concat((F.relu(x_true - a0_hat), F.relu(a0_hat - x_true)), axis=1)
        else:
            errors  = self.e0
        
        return errors/(batchsize * n_channels * h * h) , a0_hat
