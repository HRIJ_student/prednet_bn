import code
import sys

import numpy
import chainer
import chainer.functions as F
import chainer.links as L

from batch_norm import BatchNormalization as BN


class ConvLSTM(chainer.Chain):
    
    def __init__(self, in_channels, out_channels, height,  width, 
                 gamma=0.1,wscale=1, nobias=False, nt=10, 
                 initialW=None, initial_bias=None):
        super(ConvLSTM, self).__init__(
            # upward, lateral connections
            X_conv=L.Convolution2D(in_channels, out_channels*4, 3, 
                                   nobias=True, pad=1),
            h_conv=L.Convolution2D(out_channels, out_channels*4, 3, 
                                   nobias=True, pad=1),

            # batch norm links for external, recurrent, and cell-state inputs
            bnr_X=BN(out_channels*4, use_beta=False, nt=nt,
                    initial_gamma=(numpy.ones(out_channels*4)
                                   * gamma).astype(numpy.float32)),

            bnr_h=BN(out_channels*4, use_beta=False, nt=nt,
                    initial_gamma=(numpy.ones(out_channels*4)
                                   * gamma).astype(numpy.float32)),

            bnr_c=BN(out_channels, nt=nt,
                    initial_gamma=(numpy.ones(out_channels)
                                   * gamma).astype(numpy.float32)),
            # bias
            bias=L.Bias(shape=(out_channels*4, height, width)),
        )
        self.in_channels  = in_channels
        self.out_channels = out_channels
                
        self.reset_state()
        
    def reset_state(self):
        self.c = self.h = None
            
    def __call__(self, x, timestep,test=False): 
        batchsize, in_channels, height, width = x.data.shape

        # BN(W_x*x)
        h = self.bnr_X(self.X_conv(x), timestep,test=test) 

        # BN(W_x*x) + BN(W_h*h)
        if self.h is not None: 
            h += self.bnr_h(self.h_conv(self.h), timestep,test=test)

        # BN(W_x*x) + BN(W_h*h) + b
        h = self.bias(h)

        # split conv into forget, input, output, and cell gate
        f, i, o, g = F.split_axis(h, 4, 1) # (4,192,16,20)

                
        if self.c is None:
            self.c = chainer.Variable(self.xp.zeros(
                (batchsize,self.out_channels,height,width), 
                 dtype=x.data.dtype), volatile='auto')

        self.c = F.sigmoid(f) * self.c + F.sigmoid(i) * F.tanh(g)
        self.h = F.sigmoid(o) * F.tanh(self.bnr_c(self.c, timestep,test=test))

        #code.interact(local=dict(globals(), **locals()))
        #sys.exit()
        return self.h
                    
