#!/usr/bin/env python

from __future__ import print_function
import argparse
import numpy as np
import os
import math
import sys
import cPickle as pickle
import time
import shutil

import chainer
import chainer.functions as F
import chainer.links as L
from chainer import cuda
from chainer import optimizers
from chainer import serializers

import prednet
import data_utils
from path_settings import *

parser = argparse.ArgumentParser()
parser.add_argument('--batchsize',       '-b', default=4,   type=int, help='batch size')
parser.add_argument('--initmodel',       '-m', default='prednet.model', help='Initialize the model from given file')
parser.add_argument('--resume',          '-r', default='',            help='Resume the optimization from snapshot')
parser.add_argument('--gpu',             '-g', default=-1,  type=int, help='GPU ID (negative value indicates CPU)')
parser.add_argument('--rng_seed',        '-e', default=123456, type=int, help='seed for random number generator')
args = parser.parse_args()

xp = cuda.cupy if args.gpu >= 0 else np

if not os.path.exists(RESULTS_SAVE_DIR): os.mkdir(RESULTS_SAVE_DIR)


# initialize numpy random seed
xp.random.seed(args.rng_seed)
np.random.seed(args.rng_seed)

height = 128
width = 160
n_channels = [3, 48, 96, 192]

nt = 10
n_epochs = 150
samples_per_epoch = 500
N_seq_val = 100
batch_size = args.batchsize

model = prednet.PredNet(height,  width, n_channels,nt=nt)
if args.gpu >= 0:
    cuda.get_device(args.gpu).use()
    model.to_gpu()
    

print('Load model from', MODEL_DIR)
serializers.load_npz(MODEL_DIR + args.initmodel, model)

print('Load test data')
#data_test, source_test = data_utils.load_test_data()
x_test = np.load('/home/evan/samba/data/npy/caltech_test.npy')


#possible_starts_test = data_utils.get_possible_start_times(source_test, 2*nt)
#start_times_test     = data_utils.choose_start_times(possible_starts_test, 2*nt)
#x_test = data_utils.create_batches(data_test, start_times_test, batch_size, 2*nt, batch_size*(start_times_test.shape[0]//batch_size))

#selected_sources = source_test[start_times_test]

print('x_test.shape:', x_test.shape)

losses_test  = np.zeros((x_test.shape[0]*batch_size, 2*nt))
x_nows_test  = np.zeros((x_test.shape[0], 2*nt, x_test.shape[2], x_test.shape[3], x_test.shape[4]))
x_preds_test = np.zeros((x_test.shape[0], 2*nt, x_test.shape[2], x_test.shape[3], x_test.shape[4]))
print('x_preds.shape:',x_preds_test.shape)

#for i in xrange(x_test.shape[0]):
for i in xrange(x_test.shape[0] // batch_size):
    losses  = xp.zeros((batch_size, 2*nt))
    x_nows  = xp.zeros((batch_size, 2*nt, x_test.shape[2], x_test.shape[3], x_test.shape[4]))
    x_preds = xp.zeros((batch_size, 2*nt, x_test.shape[2], x_test.shape[3], x_test.shape[4]))
    batch   = xp.copy(x_test[(i*batch_size):(i+1)*batch_size]) # (4,20,3,128,160)
    
    #input_data = x_test[i].reshape(batch_size, 2*nt, x_test.shape[2], x_test.shape[3], x_test.shape[4])
    for t in xrange(2*nt):
        x_now  = chainer.Variable(xp.asarray(batch[:, t, :, :, :]).astype(xp.float32), volatile='on')
        if t < nt:
            loss, x_pred   = model(x_now,t, test=True)
        else:
            loss, x_pred   = model(x_pred,t,test=True,x_true=x_now)
        losses[:, t] = F.sum(loss, axis=(1, 2, 3)).data/nt
        x_preds[:, t, :, :, :] = x_pred.data
        x_nows[:, t, :, :, :]  = x_now.data
    
    losses_test[i*batch_size:(i+1)*batch_size, :]  = cuda.to_cpu(losses)
    x_nows_test[i*batch_size:(i+1)*batch_size, :, :, :, :]  = cuda.to_cpu(x_nows)
    x_preds_test[i*batch_size:(i+1)*batch_size, :, :, :, :] = cuda.to_cpu(x_preds)
    model.reset_state()

np.save(RESULTS_SAVE_DIR + 'losses_test.npy',  losses_test)
#np.save(RESULTS_SAVE_DIR + 'x_nows_test.npy',  x_nows_test)
np.save(RESULTS_SAVE_DIR + 'x_preds_test.npy', x_preds_test)
#np.save(RESULTS_SAVE_DIR + 'selected_sources.npy', selected_sources)
#np.save(RESULTS_SAVE_DIR + 'x_test.npy', x_test)
mse = np.mean((x_nows_test[:,1:] - x_preds_test[:,1:])**2)
print(str(mse))


