import code
import sys

import numpy

from chainer.functions.normalization import batch_normalization
from chainer import initializers
from chainer import link
from chainer import variable


class BatchNormalization(link.Link):

    def __init__(self, size, decay=0.9, eps=2e-5, dtype=numpy.float32,
                 use_gamma=True, use_beta=True, nt=10, 
                 initial_gamma=None, initial_beta=None, use_cudnn=True):
        super(BatchNormalization, self).__init__()
        if use_gamma:
            self.add_param('gamma', size, dtype=dtype)
            if initial_gamma is None:
                initial_gamma = initializers.One()
            initializers.init_weight(self.gamma.data, initial_gamma)
        if use_beta:
            self.add_param('beta', size, dtype=dtype)
            if initial_beta is None:
                initial_beta = initializers.Zero()
            initializers.init_weight(self.beta.data, initial_beta)
        for timestep in xrange(nt):
            self.add_persistent('avg_mean_' + str(timestep), numpy.zeros(size,dtype=dtype))
            self.add_persistent('avg_var_' + str(timestep), numpy.zeros(size,dtype=dtype))

        #self.add_persistent('avg_mean', numpy.zeros((nt,size), dtype=dtype))
        #self.add_persistent('avg_var', numpy.zeros((nt,size), dtype=dtype)) 
        self.add_persistent('N', 0)
        self.decay = decay
        self.eps = eps
        self.use_cudnn = use_cudnn
        self.size = size

    def __call__(self, x, timestep, test=False, finetune=False):

        if hasattr(self, 'gamma'):
            gamma = self.gamma
        else:
            gamma = variable.Variable(self.xp.ones(
                self.size, dtype=x.dtype), volatile='auto')
        if hasattr(self, 'beta'):
            beta = self.beta
        else:
            beta = variable.Variable(self.xp.zeros(
                self.size, dtype=x.dtype), volatile='auto')
        

        if not test: 
            avg_mean = getattr(self, 'avg_mean_' + str(timestep))
            avg_var  = getattr(self, 'avg_var_' + str(timestep))
            if finetune:
                self.N += 1
                decay = 1. - 1. / self.N
            else:
                decay = self.decay
            
            func = batch_normalization.BatchNormalizationFunction(
                self.eps, avg_mean, avg_var, 
                True, decay, self.use_cudnn)
            ret = func(x, gamma, beta)

            setattr(self, 'avg_mean_' + str(timestep), func.running_mean)
            setattr(self, 'avg_var_' + str(timestep), func.running_var)
            #avg_mean = func.running_mean 
            #avg_var  = func.running_var

            #code.interact(local=dict(globals(), **locals()))
            #sys.exit()
            #self.avg_var[timestep] = func.running_var
            #print('%d %f %f' %(timestep,self.avg_mean[0][0],self.avg_mean[1][0]))

        else:
            # Use running average statistics or fine-tuned statistics.
            mean = variable.Variable(self.avg_mean_9, volatile='auto')
            var = variable.Variable(self.avg_var_9, volatile='auto')
            ret = batch_normalization.fixed_batch_normalization(
                x, gamma, beta, mean, var, self.eps, self.use_cudnn)
        return ret

    def start_finetuning(self):
        """Resets the population count for collecting population statistics.

        This method can be skipped if it is the first time to use the
        fine-tuning mode. Otherwise, this method should be called before
        starting the fine-tuning mode again.

        """
        self.N = 0
